import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Dashboard from '../views/Dashboard.vue'
import VueCookies from 'vue-cookies'
import axios from 'axios'

Vue.use(VueRouter)

const routes = [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/',
        name: 'home',
        redirect: '/login'
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

// Middleware Style
router.beforeEach((to, from, next) => {

    axios.get('config.json')
        .then(response => {
            const backend = response.data.backend
            // Redirect to login page if not logged in and trying to access a restricted page
            const publicRoutes = ["/login"]
            const privateRoutes = !publicRoutes.includes(to.path)
            let token = VueCookies.get('token')
            if (privateRoutes && !token) {
                return next("/login")
            } else if (privateRoutes && token) {
                const adapter = axios.create({
                    baseURL: backend,
                    headers: {
                        Authorization: 'Bearer ' + token,
                    },
                });
                // eslint-disable-next-line no-console
                adapter.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
                adapter.defaults.headers.common['Authorization'] = 'Bearer ' + token
                adapter
                    .get("/")
                    // eslint-disable-next-line no-unused-vars
                    .then(response => {
                        if (response.data === "ok") next()
                    })
                    // eslint-disable-next-line no-unused-vars
                    .catch(err => {
                        VueCookies.remove('token', '/', this.$domain)
                        return next("/login")
                    });
            }
            return next();
        })
});

export default router
