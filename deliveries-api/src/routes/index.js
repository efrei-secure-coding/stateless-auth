module.exports = (app) => {

    const verify = require('../controllers/LDAP').verify
    const cors = require('cors')
    const deliveries = require('../data/index')

    app.use(function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        res.setHeader('Access-Control-Allow-Headers', 'Authorization Origin, X-Requested-With, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next()
    });

    app.options('*', cors())
    app.all('*', verify)

    app.get('/', (req, res) =>  res.send(deliveries))

}
