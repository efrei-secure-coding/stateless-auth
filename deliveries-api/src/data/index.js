
const deliveries = [
    {
        at: "Luna Park, Moon",
        to: "Sal",
        content: "Prizes for the claw crane",
        crew: [
            "Amy", "Bender", "Fry", "Leela"
        ]
    },
    {
        at: "Chapek 9",
        to: "Robots of Chapek 9",
        content: "Lug nuts",
        crew: [
             "Bender", "Fry", "Leela"
        ]
    },
    {
        at: "Trisol",
        to: "Emperor Bont",
        content: "A sign",
        crew: [
            "Bender", "Fry", "Leela", "Zoidberg", "Amy"
        ]
    },
    {
        at: "Sicily 8",
        to: "Possibly Big Vinnie",
        content: "Subpoenas",
        crew: [
            "Bender", "Fry", "Leela"
        ]
    },
    {
        at: "Cannibalon",
        to: "Cannibalon peoples",
        content: "Unknown",
        crew: [
            "Bender", "Fry", "Leela"
        ]
    },
    {
        at: "Mars University",
        to: "Prof. Farnsworth",
        content: "Guenter",
        crew: [
            "Bender", "Fry", "Leela", "Prof. Farnsworth"
        ]
    }
]

module.exports = deliveries
