const service = 'main'
const config = require('./src/config')
const log = require('./src/utils/log').logService(service)
const express = require('express');
const bodyParser = require('body-parser');
const app = express();


// Load middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

require('./src/routes/index')(app);

server = app.listen(config.port, () => log.info('Serving on port ' + config.port))
