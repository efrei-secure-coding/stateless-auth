const jwt = require('../utils/jwt');
const service = '[controller] LDAP'
const log = require('../utils/log').logService(service)
const env = require('../utils/env').loadEnv
env()

exports.verify = (req, res, next) => {

    // Preflight CORS Authorization
    if ('OPTIONS' === req.method){
        res.status(204)
        next()
    }
    // No token
    else if(!req.headers) {
        log.warn("No token provided")
        res.status(401).send({"error": "no_credentials", "error_description": "You must be authenticated"})
    }

    // Token verified
    else if (jwt.verifyToken(req.headers.authorization) ) {
        log.info("Token verification succeeded for token : ",  req.headers.authorization)
        next()
    }

    // Invalid token
    else {
        log.warn("Invalid token for : ",  req.headers.authorization)
        res.status(403).send({"error": "protected_resource", "error_description": "Token not accepted"})
    }
};
