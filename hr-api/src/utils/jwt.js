const jwt = require('jsonwebtoken')

require('dotenv').config()

// Verify token
exports.verifyToken = (token) => {
    if (!token) return undefined
    return jwt.verify(token.replace('Bearer ', ''), process.env.JWT_SECRET ,(err, decoded) => {
        if (!decoded) return undefined
        if (decoded.ou === 'Office Management') return decoded
        return undefined
    })
};
