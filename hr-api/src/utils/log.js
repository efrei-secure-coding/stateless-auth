const env = require('./env').loadEnv
env()


// TODO : Clean function with currying
exports.logService = (service) => {
    const Logger = require('logplease');
    const logger = Logger.create(service);
    Logger.setLogLevel(process.env.LOG_LEVEL);
    return logger
}


