
const hr = [
    {
        name: "Dr. Amy Wong",
        age: 40,
        birth: "22 December 2979",
        species: "Human",
        planetOrigin: "Mars",
        profession: "Long-term Intern",
        img: "https://theinfosphere.org/images/thumb/f/f4/Amy_promo_2.jpg/338px-Amy_promo_2.jpg"
    },
    {
        name: "Bender Bending Rodriguez",
        age: 24,
        birth: "2996",
        species: "Robot",
        planetOrigin: "Earth",
        profession: "Assistant Manager of Sales",
        img: "https://theinfosphere.org/images/thumb/1/14/Bender_promo_2.jpg/375px-Bender_promo_2.jpg"

    },
    {
        name: "Dr. John A. Zoidberg",
        age: 92,
        birth: "2942",
        species: "Decapodian",
        planetOrigin: "Decapod 10",
        profession: "Staff doctor",
        img: "https://theinfosphere.org/images/thumb/a/a2/Zoidberg_promo_2.jpg/338px-Zoidberg_promo_2.jpg"
    },
    {
        name: "Philip J. Fry",
        age: 25,
        birth: "14 August 1974",
        species: "Human",
        planetOrigin: "Earth",
        profession: "Executive delivery",
        img: "https://theinfosphere.org/images/thumb/0/0d/Fry_promo_2.jpg/338px-Fry_promo_2.jpg"
    },
    {
        name: "Hermes Conrad",
        age: 61,
        birth: "2959",
        species: "Human",
        planetOrigin: "Earth",
        profession: "Bureaucrat and Accountant",
        img: "https://theinfosphere.org/images/thumb/4/48/Hermes_promo_2.jpg/338px-Hermes_promo_2.jpg"
    },
    {
        name: "Turanga Leela",
        age: 44,
        birth: "July 29 2975",
        species: "Mutant",
        planetOrigin: "Earth",
        profession: "Space captain",
        img: "https://theinfosphere.org/images/thumb/4/4e/Leela_promo_2.jpg/338px-Leela_promo_2.jpg"
    },
    {
        name: "Professor Hubert J. Farnsworth",
        age: 178,
        birth: "9 April 2841",
        species: "Human",
        planetOrigin: "Earth",
        profession: "Owner/CEO",
        img: "https://theinfosphere.org/images/thumb/6/66/Farnsworth_promo_2.jpg/338px-Farnsworth_promo_2.jpg"
    },
    {
        name: "Scruffy",
        age: 63,
        birth: "2957",
        species: "Zombie",
        planetOrigin: "Earth",
        profession: "Janitor",
        img: "https://theinfosphere.org/images/thumb/9/96/Scruffy_promo.jpg/338px-Scruffy_promo.jpg"
    },

]

module.exports = hr
