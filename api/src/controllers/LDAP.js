const jwt = require('../utils/jwt');
const service = '[controller] LDAP'
const log = require('../utils/log').logService(service)
const env = require('../utils/env').loadEnv
env()

exports.sign = (req, res) => {
    let info = {
        cn: req.user.cn,
        name: req.user.displayName,
        description: req.user.description,
        employeeType: req.user.employeeType,
        mail: req.user.mail,
        ou: req.user.ou
    }
    let date = new Date();
    let ttl = date.setTime(date.getTime() + 2 + parseInt(process.env.JWT_TTL) * 60 * 60 * 1000);
    let token = jwt.generateToken(info, date)
    log.info("Signed a token for user : ", req.user.mail)
    res.send({token: token});
};

exports.verify = (req, res, next) => {

    // Exceptions
    if (req.path === '/login'){
        next()
    }
    // Preflight CORS Authorization
    else if ('OPTIONS' === req.method){
        res.status(204)
        next()
    }
    // No token
    else if(!req.headers) {
        log.warn("No token provided")
        res.status(401).send({"error": "no_credentials", "error_description": "You must be authenticated"})
    }

    // Token verified
    else if (jwt.verifyToken(req.headers.authorization) || req.path ==='/login') {
        log.info("Token verification succeeded for token : ",  req.headers.authorization)
        next()
    }

    // Invalid token
    else {
        log.warn("Invalid token for : ",  req.headers.authorization)
        res.status(403).send({"error": "protected_resource", "error_description": "Token not accepted"})
    }
};
