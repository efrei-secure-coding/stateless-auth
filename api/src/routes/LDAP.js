module.exports = (app) => {

    const passport          = require('passport');
    const LdapStrategy      = require('passport-ldapauth').Strategy;
    const LDAP              = require('../controllers/LDAP.js');

    app.use(function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        res.setHeader('Access-Control-Allow-Headers', 'Authorization Origin, X-Requested-With, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next()
    });

    passport.use(new LdapStrategy({
        server: { url: 'ldap://openldap:389',
            bindDN: 'cn=admin,dc=planetexpress,dc=com',
            bindCredentials: 'GoodNewsEveryone',
            searchBase: 'OU=people,dc=planetexpress,dc=com',
            searchFilter: '(uid={{username}})'
        }
    }));

    app.use(passport.initialize());
    app.post('/login', passport.authenticate('ldapauth', {session: false}), function(req, res){LDAP.sign(req, res)})

};
