
module.exports = (app) => {

    const verify = require('../controllers/LDAP').verify
    const cors = require('cors')

    app.options('*', cors())
    app.all('*', verify)

    app.get('/', (req, res) =>  res.send("ok") )

}
