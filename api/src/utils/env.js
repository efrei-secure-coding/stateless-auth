// Load variable from .env or from environment variable in production
exports.loadEnv = () => {

    let requiredEnv = [
        'JWT_TTL',
        'JWT_SECRET'
    ]

    if (process.env.NODE_ENV !== 'production') {
        require('dotenv').config()
    }


    if (process.env.LOG_LEVEL === 'undefined') {
        process.env.LOG_LEVEL = (process.env.NODE_ENV !== 'production') ? 'WARN' : 'DEBUG'
    }

    let unsetEnv = requiredEnv.filter((env) => !(typeof process.env[env] !== 'undefined'));
    if (unsetEnv.length > 0) {
        throw  new Error("Required ENV variables are not set: [" + unsetEnv.join(', ') + "]");
    }
}