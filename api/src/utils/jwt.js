const jwt = require('jsonwebtoken')

require('dotenv').config()

// Generate Token with an expiration
exports.generateToken = (info) => {
    return jwt.sign(info, process.env.JWT_SECRET, {expiresIn: process.env.JWT_TTL})
};

// Verify token
exports.verifyToken = (token) => {
    return jwt.verify(token.replace('Bearer ', ''), process.env.JWT_SECRET, process.env.JWT_TTL, (err, decoded) => {
        return decoded
    })
};
